require 'rubygems'
require 'bundler'
Bundler.require
require 'faye'
require 'bundler/setup'
require 'yaml'
require 'private_pub'
require 'redis'

Faye::WebSocket.load_adapter('thin')

PrivatePub.load_config(File.expand_path("../config/private_pub.yml", __FILE__), ENV["RACK_ENV"] || "test")

redis_client = Redis.new(:url => PrivatePub.config[:redis_url])
app = PrivatePub.faye_app

app.on(:subscribe) do |client_id, channel|
  if channel.include? '/user/'
    puts 'setting '+ channel.delete('/user/') + client_id
    redis_client.hset channel.delete('/user/'), client_id, true
  end
end

app.on(:unsubscribe) do |client_id, channel|
  if channel.include? '/user/'
    puts 'deleting' + channel.delete('/user/') + client_id
    redis_client.hdel channel.delete('/user/'), client_id
  end
end

run app
